import 'tsconfig-paths/register';

import { ConnectionOptions } from 'typeorm';
import { User } from '@app/entities';
import { DatesModel } from '@entities/Dates/Dates.model';

const entities = [User, DatesModel];

const typeORMConfig: ConnectionOptions = {
  type: 'mysql',
  host: process.env.DB_HOST || 'localhost',
  port: 3306,
  username: 'test',
  password: 'test',
  database: 'test',
  synchronize: true,
  logging: true,
  entities
};

export = typeORMConfig;

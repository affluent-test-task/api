import { Connection, getConnectionManager } from 'typeorm';
import * as express from 'express';
require('dotenv').config();
require('tsconfig-paths/register');

import { Express } from 'express';

const typeORMConfig = require('@app/ormconfig');
import { UserParser } from '@entities/User/User.parser';
import * as http from 'http';
import * as bodyParser from 'body-parser';
import { DatesController, UserController } from '@app/entities';
import { DatesParser } from '@entities/Dates/Dates.parser';

export class AffluentTestApi {
  public server: Express;
  public httpServer: http.Server;

  private connection?: http.Server;
  private baseAPIPath: string = '/api';
  public mySQLConnection?: Connection;

  constructor() {
    this.server = express();
    this.httpServer = http.createServer(this.server);
  }

  private async connectToDatabase() {
    try {
      this.mySQLConnection = getConnectionManager().create(typeORMConfig);

      await this.mySQLConnection.connect();
      console.log('MySQL DB successfully connected');
    } catch (err) {
      console.error(err);
      setTimeout(this.connectToDatabase.bind(this), 3000);
    }
  }

  public async start() {
    this.registerShutdownHooks();
    this.setupBodyParser();
    this.registerControllers();

    await this.connectToDatabase();
    await new UserParser().getData();
    await new DatesParser().getData();

    this.server!.use('/public', express.static('public'));

    await this.startServer(8080);
  }

  public async stop() {
    if (this.connection) {
      return new Promise((resolve, reject) => {
        this.connection!.close((err?: Error) => {
          err ? reject(err) : resolve();
        });
      });
    }

    if (this.mySQLConnection) {
      await this.mySQLConnection.close();
      console.log('Connection to database closed');
    }

    return;
  }

  private startServer(port: number): Promise<void> {
    return new Promise(resolve => {
      this.connection = this.httpServer.listen(port, () => {
        console.log(`Express server connected on ${port}`);
        resolve();
      });
    });
  }

  private registerShutdownHooks() {
    let gracefulShutdown = () =>
      this.stop()
        .then(() => console.log('Stopping service'))
        .then(() => process.exit(), () => process.exit());

    process.on('SIGTERM', gracefulShutdown);
    process.on('SIGINT', gracefulShutdown);
  }

  private setupBodyParser() {
    this.server.use(bodyParser.urlencoded({ extended: false, limit: '50mb' }));
    this.server.use(bodyParser.json({ limit: '50mb' }));
    this.server.use(bodyParser.text({ limit: '50mb' }));
  }

  private registerControllers() {
    this.server.use(this.baseAPIPath, UserController);
    this.server.use(this.baseAPIPath, DatesController);
  }
}

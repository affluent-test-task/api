import * as puppeteer from 'puppeteer';
import { DatesModel } from '@app/entities/Dates/Dates.model';
import { getRepository } from 'typeorm';

export class DatesParser {
  private baseUrl: string = 'https://publisher-dev.affluent.io/login';
  private getBrowser = async () => await puppeteer.launch();
  private devLogin: string = 'developertest@affluent.io';
  private devPassword: string = 'H3lloWorld!';
  private rawData: string[][] = [];
  private parsedResult: DatesModel[] = [];

  private parsePage = 'https://publisher-dev.affluent.io/list?type=dates';

  public async getData() {
    const shouldRun = await this.shouldRun();
    if (!shouldRun) {
      return;
    }
    await this.getRawData();
    this.processResult();
    await this.saveResult();
  }

  private async getRawData() {
    const browser = await this.getBrowser();
    const page = await browser.newPage();

    await page.goto(this.baseUrl);

    await page.type("input[name='username']", this.devLogin);
    await page.type("input[name='password']", this.devPassword);
    await page.click('.uppercase');
    await page.waitForNavigation();

    await page.goto(this.parsePage);

    await page.waitFor(2000);

    await page.click('div[id="dashboard-report-range"]');
    await page.waitFor(2000);
    async function formatDate(selector: string, newDate: string) {
      await page.$eval(selector, element => {
        element.value = '';
      });

      await page.type(selector, newDate);
    }
    await formatDate("input[name='daterangepicker_start']", '08/01/2019');
    await formatDate("input[name='daterangepicker_end']", '08/31/2019');
    await page.click('.btn-success');

    await page.waitFor(2000);

    async function evalData(): Promise<string[][]> {
      return (await page.$$eval("table[id='DataTables_Table_0'] > tbody > tr", trs => {
        return trs.map(item => {
          return Array.from(item.cells).map((cell: any): string => cell.innerText);
        });
      })) as any;
    }

    const data = await evalData();

    this.rawData.push(...data);

    const canNext = await page.$eval("li[class='next'] > a[title='Next']", elem => {
      return elem !== null;
    });

    if (canNext) {
      await page.click("li[class='next'] > a[title='Next']");
      await page.waitFor(1000);

      const secondData = await evalData();
      this.rawData.push(...secondData);
    }
  }

  private processResult() {
    this.parsedResult = this.rawData.map(result => {
      const parsedResult = new DatesModel();

      parsedResult.date = result[0];
      parsedResult.commissionsTotal = result[1];
      parsedResult.salesNet = parseFloat(result[2]);
      parsedResult.leadsNet = parseFloat(result[3]);
      parsedResult.clicks = parseInt(result[4]);
      parsedResult.EPC = result[5];
      parsedResult.impressions = parseFloat(result[6]);
      parsedResult.CR = result[7];

      return parsedResult;
    });
  }

  private async shouldRun() {
    const count = await getRepository(DatesModel).count();
    return count === 0;
  }

  private async saveResult() {
    const repository = getRepository(DatesModel);
    await repository.save(this.parsedResult);
  }
}

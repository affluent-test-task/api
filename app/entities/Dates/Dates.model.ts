import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class DatesModel {
  @PrimaryGeneratedColumn('uuid')
  public id: string;

  @Column()
  public date: string;

  @Column()
  public commissionsTotal: string;

  @Column()
  public salesNet: number;

  @Column()
  public leadsNet: number;

  @Column()
  public clicks: number;

  @Column()
  public EPC: string;

  @Column()
  public impressions: number;

  @Column()
  public CR: string;
}

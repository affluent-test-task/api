import * as express from 'express';

import { getRepository } from 'typeorm';
import { DatesModel } from '@app/entities/Dates/Dates.model';

const DatesController = express.Router();

DatesController.route('/dates').get(async (req, res, next) => {
  try {
    const result = await getRepository(DatesModel).find();

    return res.json(result);
  } catch (err) {
    console.log(err);
    return next(err);
  }
});

export { DatesController };

import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

export interface IFetchedUserData {
  id: string;
  email: string;
  first_name: string;
  last_name: string;
  avatar: string;
}

@Entity()
export class User {
  @PrimaryGeneratedColumn('uuid')
  public id: string;

  @Column({ nullable: false })
  public email: string;

  @Column()
  public firstName: string;

  @Column()
  public lastName: string;

  @Column()
  public avatar: string;

  public static create(inputData: IFetchedUserData): User {
    const result = new User();
    const { id, email, avatar, first_name, last_name } = inputData;

    result.id = id;
    result.email = email;
    result.avatar = avatar;
    result.firstName = first_name;
    result.lastName = last_name;

    return result;
  }
}

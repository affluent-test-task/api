import { getRepository } from 'typeorm';
const axios = require('axios');

import { IFetchedUserData, User } from '@entities/User/User.model';

export class UserParser {
  private baseUrl: string = 'https://reqres.in/api/users?page=';

  public async getData() {
    const firstPageResultUsers = await this.fetchPage();
    const secondPageResultUser = await this.fetchPage('2');

    return UserParser.recordData([...firstPageResultUsers, ...secondPageResultUser]);
  }

  private async fetchPage(page: string = '1'): Promise<User[]> {
    const uri = `${this.baseUrl}${page}`;
    const response = await axios.default.get(uri);

    const data: IFetchedUserData[] = response.data.data;

    return data.map(User.create);
  }

  private static async recordData(data: User[]) {
    const userRepository = getRepository(User);
    const existElements = await userRepository.find();
    if (existElements.length === 12) {
      return;
    }
    await userRepository.save(data);
  }
}

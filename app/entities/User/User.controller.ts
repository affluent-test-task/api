import * as express from 'express';

import { getRepository } from 'typeorm';
import { User } from '@entities/User/User.model';

const UserController = express.Router();

UserController.route('/users').get(async (req, res, next) => {
  try {
    const users = await getRepository(User).find();

    return res.json(users);
  } catch (err) {
    console.log(err);
    return next(err);
  }
});

export { UserController };

FROM node:12-alpine

WORKDIR /api

COPY package.json ./
COPY yarn.lock ./
COPY tsconfig.json ./

COPY node_modules ./node_modules
COPY app ./app
COPY public ./public
COPY .env.compose ./.env

EXPOSE 8080

CMD ["yarn", "server"]